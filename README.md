# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

Implementation test adding TDD

### How do I get set up? ###

Because the project is not using external frameworks, just clone it

### Contribution guidelines ###

Suggestions? bugs? all welcome.

### The problem ###

Create an iPhone application which downloads the flights and hotel JSON files and displays the information appropriately.

What we want to see: - Good OO design

- Clean + bug free

- Unit tests

- Your command of the Objective C language 

- Good design principles and patterns

Brownie points will be given for showing us your notes, commit history and some kind of idea on how you approached the problem.

We expect you to be able to understand and decide on the exact requirements and UI for this project based on the information provided. It has been generalised for a reason. Your aim should be to complete the test based on the requirements. If you have any questions during the process feel free to ask.

### Description of the solution ###

The project is formed of these main parts:

**Models**, where the data dictionary is transformed into an appropriate model object

**Communications**, to fetch the data from external sources, in case of the flights and hotels, the conversion from JSON to NSObject is made here. The pictures are fetched asynchronously in this class as well, and stored in a separated singleton class into a NSDictionary object.

**Controllers**, to populate the views from the data and handle some user actions.

**Views**, storyboard used in the project with the needed layouts inside, plus the custom cells and a pair of UIView's, one to encapsulate a UICollectionView for the facilities and the other to encapsulate a UIScrollView for the images carrousel.

**Utils**, the images store singleton class, a NSObject with a formatter method, and a simple HUD to be implemented while fetching data and the user is waiting for some response.

A Navigation bar is on charge to grant the access to the flights layout. This bar has not been customised but to change default buttons for custom ones or to include an image logo is a minor task.

The communications between the "communications" class and the other classes is made using delegates. Except for the images downloader, this ones uses blocks.
Before download an image the image store (NSDictionary) is requested to get the image, if the image does not exist is downloaded in a dispatched thread.

There is a parent ViewController class containing the common code (show/hide HUD, alert view controller) and the constants for the error messages.

To check the internet connection the Reachability classes provided from Apple have been imported to the project. The way of using this classes is: After an error in the response check the internet connection, if there is not connection show the appropriate message. Because Reachability can fail (actually in bad network connection it does), an additional general message will be shown to the user.

The info.plist has been modified to allow insecure connections to the endpoints domain.

After fetching the data for the hotel, this is a JSON containing a unique object, just in case than in a future it become to be an array of hotels this object is add to an array and shown in a TableView, the details can be

The HUD will be shown if the delay to fetch data is more than 0.3 secs, like this way we avoid the "splash" HUD screens.

To show the rate of the hotel a simple view containing starts has been implemented in the two layouts containing the hotel information. The logic calculation is in FHFormatUtils and is performed dispatching a new thread.

### Left to do ###

Write more test units using some kind of mocking (Mockito could be a good framework for this)

The method imgWithUrl:completion return nil if the image can't be fetched, could be a good idea a default image in these cases.

### Who do I talk to? ###

Jose Catala
jfca68@gmail.com