//
//  AppDelegate.h
//  flightshotels
//
//  Created by Jose Francisco Catalá Barba on 24/03/2017.
//  Copyright © 2017 Jose Francisco Catalá Barba. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

